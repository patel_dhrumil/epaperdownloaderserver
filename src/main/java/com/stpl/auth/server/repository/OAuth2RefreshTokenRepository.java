package com.stpl.auth.server.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.stpl.auth.server.model.OAuth2AuthenticationRefreshToken;

public interface OAuth2RefreshTokenRepository extends MongoRepository<OAuth2AuthenticationRefreshToken, String> {

}